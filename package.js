Package.describe({
  summary: "toolbox for meteor projects",
});

Package.on_use(function (api) {
  api.use(['mongo-livedata', 'json', 'underscore','extend'],['client', 'server']);

  api.add_files('src/toolbox.js', ['client', 'server']);
});

Package.on_test(function (api) {
    api.use('toolbox');
    api.use('tinytest');
    api.use('test-helpers');
    api.add_files('tests/prepare.tests.js', ['client', 'server']);
    api.add_files('tests/toolbox.tests.js', ['client', 'server']);
});