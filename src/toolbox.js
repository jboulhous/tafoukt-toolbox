
(function () {

    var Toolbox = {};
    
    // _.extend(Toolbox,{VERSION : '0.0.1'});
    Toolbox.ExtValidator = (function(){
        var Validator = function(){
            this.validators = [];
            this.errors = []
        };
        
        _.extend(Validator.prototype,{
            _runOne : function(doc, func){
                var self = this;
                try{
                    func.call(self,doc)
                }catch(e){
                    self.errors.push(e.message);
                }
                return;
            },
            run:function(doc){
                var self = this;
                self.errors = [];
                _.each(self.validators,function(func){
                    self._runOne(doc, func)
                });
                if(self.errors.length>0)
                    return false
                return true;
            }
        })
        
        Validator.extend = Meteor.extend;
        
        var ExtValidator = Validator.extend({
            add : function(func){
                this.validators.push(func)
            }
        })
        
        return ExtValidator;
    })();
    
    var globals = this;
    globals.Toolbox = Toolbox;
    

})();


