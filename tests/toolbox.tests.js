(function(){





/******************************************************************************/
/* Server                                                              */
/******************************************************************************/

  if (Meteor.isServer) {

    Tinytest.add('Server - Toolbox -  Namespace', function (test) {
      if(_.isObject(Toolbox))
        test.ok(['Toolbox exists']);
    });
    
    Tinytest.add('Server - Toolbox -  ExtValidator', function (test) {
        var f = function(doc){
                if(doc.b == 0) 
                    throw new Error("b field equals 0");
                return true ;
            },
            f1 = function(doc){
                if(doc.a == 100) 
                    throw new Error("a field is too big");
                doc.a = 1 ; 
                return true ;
            },
            exec = new Toolbox.ExtValidator();
        
        _.each([f1, f],function(func){
            exec.add(func);
        });
        var doc1 = { a:0, b:0 }, doc2 = {a:0}, doc3 = { a:100, b:0 } ;
        if(exec.run(doc1)){
            test.fail(["doc1 invalid"])
        } else{
            test.ok(["doc1 invalid"])
        }
        test.length(exec.errors,1);
        test.equal(exec.errors[0],"b field equals 0")
        
        if(exec.run(doc2)){
            test.ok(["doc2 valid"])
        } else{
            test.ok(["doc2 valid"])
        }
        test.equal(exec.errors.length,0);
        
        if(exec.run(doc3)){
            test.fail(["doc3 invalid"])
        } else{
            test.ok(["doc3 invalid"])
        }
        test.length(exec.errors,2);
        test.equal(exec.errors[0],"a field is too big")
        test.equal(exec.errors[1],"b field equals 0")
        
    });


  };

/******************************************************************************/
/* Client                                                                 */
/******************************************************************************/

  if (Meteor.isClient) {
      
    Tinytest.add('Client - Toolbox -  Namespace', function (test) {
      if(_.isObject(Toolbox))
        test.ok(['Toolbox exists']);
    });
    
    Tinytest.add('Client - Toolbox -  ExtValidator', function (test) {
        var f = function(doc){
                if(doc.b == 0) 
                    throw new Error("b field equals 0");
                return true ;
            },
            f1 = function(doc){
                if(doc.a == 100) 
                    throw new Error("a field is too big");
                doc.a = 1 ; 
                return true ;
            },
            exec = new Toolbox.ExtValidator();
        
        _.each([f1, f],function(func){
            exec.add(func);
        });
        var doc1 = { a:0, b:0 }, doc2 = {a:0}, doc3 = { a:100, b:0 } ;
        if(exec.run(doc1)){
            test.fail(["doc1 invalid"])
        } else{
            test.ok(["doc1 invalid"])
        }
        test.length(exec.errors,1);
        test.equal(exec.errors[0],"b field equals 0")
        
        if(exec.run(doc2)){
            test.ok(["doc2 valid"])
        } else{
            test.ok(["doc2 valid"])
        }
        test.equal(exec.errors.length,0);
        
        if(exec.run(doc3)){
            test.fail(["doc3 invalid"])
        } else{
            test.ok(["doc3 invalid"])
        }
        test.length(exec.errors,2);
        test.equal(exec.errors[0],"a field is too big")
        test.equal(exec.errors[1],"b field equals 0")
        
    });

 

  };

}).call();